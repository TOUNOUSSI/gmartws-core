package com.gmart.api.messages.responses.enums;

public enum SignUpStatus {
	CREATED, NOT_CREATED
}
