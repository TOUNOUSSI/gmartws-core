package com.gmart.api.messages.responses.enums;

public enum LoginStatus {
	AUTHENTICATED, NOT_AUTHENTICATED
}
