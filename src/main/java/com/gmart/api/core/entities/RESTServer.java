package com.gmart.api.core.entities;

import lombok.Data;

@Data
public class RESTServer {
	private String user;
	private String password;
	private String host;

}
